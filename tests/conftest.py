import pytest
from httpx import AsyncClient
from sqlalchemy.pool import StaticPool
from sqlmodel import create_engine, SQLModel, Session

from config.db_config import get_session
from main import app


@pytest.fixture(name="session", autouse=True)
def session_fixture():
    engine = create_engine(
        "sqlite://", connect_args={"check_same_thread": False}, poolclass=StaticPool
    )
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        yield session


async def get_country_code_locally(country_name, app):
    return 643


@pytest.fixture(scope='session')
def anyio_backend():
    return 'asyncio'


@pytest.fixture(scope='session')
async def client():
    async with AsyncClient(app=app, base_url='http://127.0.0.1:5000') as client:
        print('Client is ready')
        yield client


@pytest.fixture(name="client")
async def client_fixture(session: Session):
    def get_session_override():
        return session

    app.dependency_overrides[get_session] = get_session_override
    async with AsyncClient(app=app, base_url='http://127.0.0.1:5000') as client:
        print('Client is ready')
        yield client
    app.dependency_overrides.clear()
