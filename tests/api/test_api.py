from unittest import mock

import pytest
from httpx import AsyncClient

from tests.conftest import get_country_code_locally

DEFAULT_PHONE_NUMBER = "75556668880"


@pytest.mark.anyio
async def test_create_get_update_user(client: AsyncClient):
    data = {
        "name": "Алекс",
        "surname": "Петров",
        "patronymic": "",
        "phone_number": DEFAULT_PHONE_NUMBER,
        "country": "Россия"
    }
    response = await client.post('/save_user_data', json=data)
    assert response.status_code == 201

    data = {
        "phone_number": DEFAULT_PHONE_NUMBER
    }
    with mock.patch('services.get_country_code', get_country_code_locally):
        response = await client.post('/get_user_data', json=data)
    assert response.status_code == 200

    data = {
        "name": "Але кс",
        "surname": "Пет-ров",
        "patronymic": "",
        "phone_number": DEFAULT_PHONE_NUMBER,
        "country": "Россия"
    }
    response = await client.post('/save_user_data', json=data)
    assert response.status_code == 200


@pytest.mark.anyio
async def test_get_non_exist_user(client: AsyncClient):
    data = {
        "phone_number": "777777888888"
    }
    response = await client.post('/get_user_data', json=data)
    assert response.status_code == 404


@pytest.mark.anyio
@pytest.mark.parametrize("test_phone_numbers", ["343567874", "245637373", "43256756735"])
async def test_create_user_with_incorrect_phone_number(client: AsyncClient, test_phone_numbers: list):
    data = {
        "name": "Алексей",
        "surname": "Павлов",
        "patronymic": "Петрович",
        "phone_number": test_phone_numbers,
        "country": "Россия"
    }
    response = await client.post('/save_user_data', json=data)
    assert response.status_code == 422


@pytest.mark.anyio
async def test_create_get_delete_user(client: AsyncClient):
    phone_number = "76978987"
    data = {
        "name": "Алексей",
        "surname": "Петров",
        "patronymic": "",
        "phone_number": phone_number,
        "country": "Россия"
    }
    response = await client.post('/save_user_data', json=data)
    assert response.status_code == 201

    data = {
        "phone_number": phone_number
    }

    with mock.patch('services.get_country_code', get_country_code_locally):
        response = await client.post('/get_user_data', json=data)
    assert response.json().get('name') == "Алексей"

    data = {
        "phone_number": phone_number
    }
    response = await client.post('/delete_user_data', json=data)
    assert response.status_code == 204
