from pathlib import Path
from typing import Tuple, Union

from pydantic import BaseSettings, Field


class Settings(BaseSettings):
    APP_ENVIRONMENT: str = 'development'
    API_URL: str = 'api'
    API_PORT: int = 5000
    BASE_DIR = Path(__file__).resolve().parent.parent
    REDIS_URL: str = 'redis://redis'

    IS_DEV_MODE: bool = False
    ALLOW_ORIGINS: Union[str, Tuple[str, ...]] = Field(default_factory=tuple)

    @property
    def LOGGING_LEVEL(self):
        return 'WARNING' if self.APP_ENVIRONMENT == 'production' else 'INFO'

    @property
    def LOGGING_CONFIG(self):
        return {'loggers': {'uvicorn': {'handlers': ['default'], 'level': self.LOGGING_LEVEL, 'propagate': False}}}

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'


http_settings = Settings()
