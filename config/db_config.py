from urllib.parse import quote

from sqlmodel import SQLModel, create_engine, Session
from dotenv import load_dotenv
from pydantic import BaseSettings


load_dotenv()


class Settings(BaseSettings):

    DATABASE_NAME: str = 'test_task'
    DATABASE_HOST: str = 'db'
    DATABASE_PORT: int = None
    DATABASE_USER: str = 'postgres'
    DATABASE_PASS: str = quote('postgres')

    @property
    def DATABASE_URI(self):
        port_url = 'postgresql://{db_user}:{db_pass}@{db_host}:{db_port}' \
                   '/{db_name}'
        instance_url = 'postgresql://{db_user}:{db_pass}@{db_host}/{db_name}'
        url = port_url if self.DATABASE_PORT else instance_url

        return url.format(
            db_user=self.DATABASE_USER,
            db_pass=self.DATABASE_PASS,
            db_host=self.DATABASE_HOST,
            db_name=self.DATABASE_NAME,
            db_port=self.DATABASE_PORT,
        )

    class Config:
        env_file = '../../../.env'
        env_file_encoding = 'utf-8'


settings = Settings()

engine = create_engine(settings.DATABASE_URI, echo=True)  #


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


def get_session():
    with Session(engine) as session:
        yield session
