import redis as redis
import uvicorn
from fastapi import FastAPI, Depends
from sqlmodel import Session
from starlette.responses import Response
from starlette.status import HTTP_204_NO_CONTENT, HTTP_404_NOT_FOUND, HTTP_201_CREATED, HTTP_200_OK

from config import http_config
from config.db_config import create_db_and_tables, engine, get_session
from models import User, UserRequest, UserResponse
from services import save_user, get_user, delete_user


class Settings:
    http_api = http_config.Settings()


def create_app(_config: Settings.http_api):
    _app = FastAPI()

    @_app.post("/save_user_data", summary='Сохранение(создание/обновления данных пользователя')
    async def save_user_data(user: User, session: Session = Depends(get_session)):
        created = await save_user(session=session, user=user)

        if created:
            return Response(status_code=HTTP_201_CREATED)
        return Response(status_code=HTTP_200_OK)

    @_app.post("/get_user_data", response_model=UserResponse, summary='Получение данных пользователя')
    async def get_user_data(user_request: UserRequest, session: Session = Depends(get_session)):
        user = await get_user(session=session, user_request=user_request, app=_app)
        if user:
            return user
        return Response(status_code=HTTP_404_NOT_FOUND)

    @_app.post("/delete_user_data", summary='Удаление данных пользователя')
    async def delete_user_data(user_request: UserRequest, session: Session = Depends(get_session)):
        result = await delete_user(session=session, user_request=user_request)
        if result:
            return Response(status_code=HTTP_204_NO_CONTENT)
        return Response(status_code=HTTP_404_NOT_FOUND)

    return _app


# Create app
app = create_app(Settings.http_api)


@app.on_event('startup')
async def startup_event():
    create_db_and_tables()

    redis_connection = redis.from_url(
        Settings.http_api.REDIS_URL,
        decode_responses=True,
        encoding='utf8',
    )
    app.state.redis = redis_connection


@app.on_event('shutdown')
async def shutdown_event():
    await app.state.redis.close()


if __name__ == '__main__':
    uvicorn.run('main:app', port=Settings.http_api.API_PORT, reload=True)
