import re
from datetime import datetime
from typing import Optional, TypeVar

from pydantic import EmailStr, validator, BaseModel
from sqlmodel import SQLModel, Field

PhoneNumber = TypeVar('PhoneNumber', bound=str)


class UserRequest(BaseModel):
    phone_number: PhoneNumber


class User(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True, exclude=True)
    name: str = Field(max_length=50)
    surname: str = Field(max_length=50)
    patronymic: Optional[str] = Field(max_length=50, default=None)
    phone_number: PhoneNumber = Field(index=True, unique=True)
    email: Optional[str] = Field(default=None)
    country: str = Field(max_length=50)
    date_created: datetime = Field(exclude=True, default_factory=datetime.now)
    date_modified: datetime = Field(exclude=True, default_factory=datetime.now)

    @validator('phone_number')
    def phone_number_must_start_from_7(cls, value):
        if value.startswith('7'):
            return value
        raise ValueError('phone number must start from 7')

    @validator('name', 'surname', 'patronymic', 'country')
    def string_contain_only_cyrillic(cls, value):
        if not value or has_cyrillic(value):
            return value
        raise ValueError('string can only contain Cyrillic and "-" " "')


class UserResponse(User):
    country_code: Optional[int] = None


def has_cyrillic(text):
    return bool(re.search('[а-яА-Я- ]', text))
