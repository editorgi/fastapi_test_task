from datetime import datetime
from typing import Optional

import redis.asyncio as redis
from dadata import Dadata
from fastapi import FastAPI
from sqlmodel import Session, select

from models import User, UserRequest, UserResponse

DADATA_KEY: str = "2299ac547270e569dee6a06e3d892aadb885ab6e"


async def save_user(session: Session, user: User) -> bool:
    statement = select(User).where(User.phone_number == user.phone_number)
    results = session.exec(statement)
    user_db = results.one_or_none()

    if user_db is None:
        session.add(user)
        created = True
        session.commit()
        session.refresh(user)
    else:
        user_db.name = user.name
        user_db.surname = user.surname
        user_db.patronymic = user.patronymic
        user_db.email = user.email
        user_db.country = user.country
        user_db.date_modified = datetime.now()
        session.add(user_db)
        created = False
        session.commit()
        session.refresh(user_db)

    return created


async def get_user(session: Session, user_request: UserRequest, app: FastAPI):
    statement = select(User).where(User.phone_number == user_request.phone_number)
    results = session.exec(statement)
    user_db = results.one_or_none()

    if not user_db:
        return

    user = UserResponse(**user_db.dict())
    user.country_code = await get_country_code(country_name=user_db.country, app=app)
    return user


async def delete_user(session: Session, user_request: UserRequest):
    statement = select(User).where(User.phone_number == user_request.phone_number)
    results = session.exec(statement)
    user_db = results.one_or_none()

    if user_db:
        session.delete(user_db)
        session.commit()

    return user_db


async def get_country_code(country_name: str, app: FastAPI) -> int:
    redis = get_redis(app)

    code = redis.get(country_name)

    if not code:
        code = await get_country_code_from_api(country_name)
        redis.set(country_name, code)

    return code


async def get_country_code_from_api(country_name):
    dadata = Dadata(DADATA_KEY)
    result = dadata.suggest("country", country_name)
    return result[0].get("data").get("code")


def get_redis(app: FastAPI) -> Optional[redis.client.Redis]:
    return app.state.redis if hasattr(app.state, 'redis') else None
